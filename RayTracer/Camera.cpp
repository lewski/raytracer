#include "Camera.h"

void Camera::update(double dt) {
	glm::vec2 movementInput = glm::vec2(0.0f);

	if (Input::keys[Input::KEY_W]) {
		movementInput.y += 1.0f;
	}

	if (Input::keys[Input::KEY_S]) {
		movementInput.y -= 1.0f;
	}

	if (Input::keys[Input::KEY_D]) {
		movementInput.x += 1.0f;
	}

	if (Input::keys[Input::KEY_A]) {
		movementInput.x -= 1.0f;
	}

	float rotationX = -Input::mouseMove.x * dt * 0.01f;
	float rotationY = -Input::mouseMove.y * dt * 0.01f;

	glm::vec3 right = glm::cross(lookDir, up);

	glm::mat4 rotationMat = glm::rotate(glm::mat4(1.0f), rotationX, up);
	rotationMat *= glm::rotate(glm::mat4(1.0f), rotationY, right);

	lookDir = rotationMat * glm::vec4(lookDir, 1.0f);
	lookDir = glm::normalize(lookDir);

	if (movementInput.x != 0.0f || movementInput.y != 0.0f) {

		float forwardMoveAmount = movementInput.y * dt * 0.001f;
		float strafeMoveAmount = movementInput.x * dt * 0.001f;

		movementInput = glm::normalize(movementInput);

		pos += lookDir * forwardMoveAmount;
		pos += right * strafeMoveAmount;
	}

}