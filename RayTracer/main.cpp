#include <iostream>

#include "Engine.h"
#include "test.cuh"


int main() {

	Engine application;

	//Cuda::cudaTest();


	try {
		application.run();
	}
	catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}