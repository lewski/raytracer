#pragma once
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

namespace Input {
	static const int KEY_W = GLFW_KEY_W;
	static const int KEY_S = GLFW_KEY_S;
	static const int KEY_D = GLFW_KEY_D;
	static const int KEY_A = GLFW_KEY_A;

	static bool keys[255];// = { false };

	static glm::vec2 mousePos;
	static glm::vec2 prevMousePos;
	static glm::vec2 mouseMove;

	static void update() {
		mouseMove = mousePos - prevMousePos;
		prevMousePos = mousePos;
	}

	static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {

		if (action == GLFW_PRESS) {
			keys[key] = true;
		}

		if (action == GLFW_RELEASE) {
			keys[key] = false;
		}
	}

	static void cursorPosCallback(GLFWwindow* window, double xpos, double ypos) {
		mousePos.x = xpos;
		mousePos.y = ypos;
	}

}

